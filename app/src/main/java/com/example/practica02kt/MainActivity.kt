package com.example.practica02kt

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    //Declaracion de variables
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnCerrar: Button? = null
    private var txtPeso: EditText? = null
    private var txtAltura: EditText? = null
    private var lblImc: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /// NOTA: CORRREGIR EL RECURSO DE LAS R Y CHECAR EL NOMBRE DE CADA BOTON
        //Relación de los botones de java con los views xml
        btnCalcular = findViewById<View>(R.id.btnCalcular) as Button
        btnLimpiar = findViewById<View>(R.id.btnLIMPIAR) as Button
        btnCerrar = findViewById<View>(R.id.btnCERRAR) as Button
        //Relación de los txt de java con los views xml
        txtAltura = findViewById<View>(R.id.txtAlt) as EditText
        txtPeso = findViewById<View>(R.id.txtPes) as EditText
        lblImc = findViewById<View>(R.id.lblResultado) as TextView


        //Codificar el evento click del boton Calcular
        btnCalcular!!.setOnClickListener {
            //Relacion con variables txt:
            val peso = txtPeso!!.text.toString()
            val altura = txtAltura!!.text.toString()
            //Declaracion de variables para la conversion
            val P = peso.toFloat()
            val A = altura.toFloat()
            //CALCULO DEL IMC:
            val imc: Float


            if (txtPeso!!.text.toString().isEmpty() || txtAltura!!.text.toString().isEmpty()
            ) {
                //Falto capturar campos
                Toast.makeText(this, "Rellena campos vacios!", Toast.LENGTH_SHORT).show()
            } else {
                imc = P / (A * A)
                val imcFormated = String.format("%.1f",imc)
                lblImc!!.text = java.lang.Float.toString(imc) //imprimir dato
            }
        }

        //Codificar el evento click del boton limpiar
        btnLimpiar!!.setOnClickListener { //Aqui se realiza la programacion
            txtAltura!!.setText("")
            txtPeso!!.setText("")
        }
        //Codificar el evento click del boton limpiar
        btnCerrar!!.setOnClickListener { //Aqui se realiza la programacion
            finish()
        }
    }
}